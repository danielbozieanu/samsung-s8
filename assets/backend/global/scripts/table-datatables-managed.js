var TableDatatablesManaged = function () {

    var initEnrollmentsTable = function () {

        var table = $('#enrollmentsTable');

        // begin first table
        table.dataTable({
            dom: 'Bfrtip',
            "scrollX": true,
            buttons: [
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i> XLS',
                    className: 'btn green-jungle btn-outline',

                },
                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-align-justify" aria-hidden="true"></i> CSV',
                    className: 'btn green-jungle btn-outline',
                },
            ],
            "responsive": "true",
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nu exista date de afisat",
                "info": "De la _START_ pana la _END_ din total de _TOTAL_",
                "infoEmpty": "Nu s-a gasit nimic",
                "infoFiltered": "(din _MAX_ inregistrari)",
                "lengthMenu": "Afiseaza _MENU_",
                "search": "Cauta:",
                "zeroRecords": "Nu s-a gasit nimic",
                "paginate": {
                    "previous": "Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, 50, 100, -1],
                [10, 20, 30, 50, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "searchable": true,
                    "targets": [2]
                },
            ],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

    };

    var initHousesTable = function () {

        var table = $('#housesTable');

        // begin first table
        table.dataTable({
            dom: 'Bfrtip',
            "scrollX": true,
            buttons: [
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i> XLS',
                    className: 'btn green-jungle btn-outline',

                },
                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-align-justify" aria-hidden="true"></i> CSV',
                    className: 'btn green-jungle btn-outline',
                },
            ],
            "responsive": "true",
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nu exista date de afisat",
                "info": "De la _START_ pana la _END_ din total de _TOTAL_",
                "infoEmpty": "Nu s-a gasit nimic",
                "infoFiltered": "(din _MAX_ inregistrari)",
                "lengthMenu": "Afiseaza _MENU_",
                "search": "Cauta:",
                "zeroRecords": "Nu s-a gasit nimic",
                "paginate": {
                    "previous": "Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, 50, 100, -1],
                [10, 20, 30, 50, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "searchable": true,
                    "targets": [0]
                },
            ],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

    };


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initEnrollmentsTable();
            initHousesTable();
        }

    };

}();

jQuery(document).ready(function () {
    TableDatatablesManaged.init();
});
