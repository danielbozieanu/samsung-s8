// override jQuery Validation to work with bootstrap
$.validator.setDefaults({
    highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'small',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent().parent('.checkbox').length) {
            error.insertAfter($(element).parent().parent());
        } else {
            error.insertAfter(element);
        }
    }
});

$.extend(jQuery.validator.messages, {
    required: "Acest camp este obligatoriu.",
    email: "Adresa de email este incorecta.",

});

// additional validation methods
$.validator.addMethod("phoneRO", function (phone_number, element) {
    phone_number = phone_number.replace(/\(|\)|\s+|-/g, "");
    return this.optional(element) || phone_number.length > 9 &&
        phone_number.match(/^(?:(?:(?:00\s?|\+)40\s?|0)(?:7\d{2}\s?\d{3}\s?\d{3}|(21|31)\d{1}\s?\d{3}\s?\d{3}|((2|3)[3-7]\d{1})\s?\d{3}\s?\d{3}|(8|9)0\d{1}\s?\d{3}\s?\d{3}))$/);
}, "Vă rugăm să introduceți un număr de telefon valid.");

$.validator.addMethod("twoWords", function (name, element) {
    return this.optional(element) || name.match(/^[a-zA-Z]*(-|\s)[a-zA-Z]*((-|\s)[a-zA-Z]*)*$/)
}, "Trebuie să introduci nume și prenume.");

// add class rules
$.validator.addClassRules("letterswithbasicpunc", {
    letterswithbasicpunc: true
});

$.validator.addClassRules("req", {
    required: true
});
