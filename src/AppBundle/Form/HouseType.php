<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class HouseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title','text', array(
            'attr' => array(
                'class' => 'form-control input-xlarge'
            )
        ))
            ->add('picture', VichFileType::class, array(
               'required' => false,
                'allow_delete' => true, // not mandatory, default is true
                'download_link' => true,
            ))
            ->add('flag', VichFileType::class, array(
               'required' => false,
                'allow_delete' => true, // not mandatory, default is true
                'download_link' => true,
            ))
            ->add('frame', VichFileType::class, array(
               'required' => false,
                'allow_delete' => true, // not mandatory, default is true
                'download_link' => true,
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\House'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_house';
    }


}
