<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints\CodeExists;

class EnrollmentFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text', array(
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('lastName', 'text', array(
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('email','email', array(
                'attr' => array(
                    'class' => 'form-control email'
                )
            ))
            ->add('phone', 'text', array(
                'attr' => array(
                    'class' => 'form-control phoneRO'
                )
            ))
            ->add('terms', CheckboxType::class, array(
                'required' => true
            ))
            ->add('pictureName', 'hidden', array(
            ))
            ->add('house', 'hidden', array(
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Enrollment'
        ));
    }

    public function getName()
    {
        return 'app_enrollment';
    }

}
