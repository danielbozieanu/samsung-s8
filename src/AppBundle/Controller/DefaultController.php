<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\EnrollmentFormType;
use AppBundle\HttpFoundation\File\Base64EncodedFile;
use AppBundle\HttpFoundation\File\UploadedBase64EncodedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $houseManager = $this->get('app.manager.house_manager');

        $houses = $houseManager->findHouses();

        return $this->render('AppBundle:Default:index.html.twig', array(
            'houses' => $houses
        ));
    }

    /**
     * @Route("/take-picture", name="take_picture")
     */
    public function takePictureAciton(Request $request)
    {
        $requestHouse = $request->request->get('selectedHouse');

        $houseManager = $this->get('app.manager.house_manager');
        $selectedHouse = $houseManager->findHouseBy(array('id' => $requestHouse));

        if ($selectedHouse == NULL) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        return $this->render('AppBundle:Default:take-picture.html.twig', array(
            'selectedHouse' => $selectedHouse
        ));
    }

    /**
     * @Route("/save-picture", name="save_picture")
     */
    public function savePictureAction(Request $request)
    {
        $requestHouse = $request->request->get('selectedHouse');
        $uploadedPhoto = $request->request->get('uploadedFileName');

        $houseManager = $this->get('app.manager.house_manager');
        $selectedHouse = $houseManager->findHouseBy(array('id' => $requestHouse));

        if ($selectedHouse == NULL || $uploadedPhoto == NULL) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        return $this->render('AppBundle:Default:save-picture.html.twig', array(
            'selectedHouse' => $selectedHouse,
            'uploadedPhoto' => $uploadedPhoto
        ));
    }


    /**
     * @Route("/upload/", name="upload", options={"expose"=true})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function uploadAction(Request $request)
    {
        $file = $request->files->get('file');

        $status = array('status' => "success", "fileUploaded" => false);

        // If a file was uploaded
        if (!is_null($file)) {
            if ($file->getClientOriginalExtension() == 'png' || $file->getClientOriginalExtension() == 'jpg') {
                // generate a random name for the file but keep the extension
                $filename = uniqid() . "." . $file->getClientOriginalExtension();
                $path = $this->getParameter('kernel.root_dir') . '/..' . $this->getParameter('upload_dir');
                $file->move($path, $filename); // move the file to a path
                $status = array('status' => "success", "fileUploaded" => true, "path" => $path, "name" => $filename);
            }
        }

        return new JsonResponse($status);
    }

    /**
     * @Route("/upload-final/", name="upload_final", options={"expose"=true})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function uploadFinalAction(Request $request)
    {
        $encoded = $request->get('image');

        $file = new UploadedBase64EncodedFile(new Base64EncodedFile($encoded));
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();

        $file->move(
            $this->getParameter('kernel.root_dir') .
            '/..' .
            $this->getParameter('upload_dir'), $fileName
        );

        $this->get('session')->set('fileName', $fileName);

        return new JsonResponse(array(
            'success' => true,
            'fileName' => $fileName
        ));
    }


    /**
     * @Route("/enrollment", name="enrollment")
     */
    public function enrollmentAction(Request $request)
    {
        $selectedHouse = $request->request->get('selectedHouse');
        $generatedPhoto = $request->request->get('finalImageName');

        $enrollmentManager = $this->get('app.manager.enrollment_manager');
        $houseManager = $this->get('app.manager.house_manager');

        $session = $this->get('session');

        if ($selectedHouse) {
            $session->set('houseSelected', $selectedHouse);
        }

        $enrollment = $enrollmentManager->createEnrollment();
        $form = $this->createForm(new EnrollmentFormType(), $enrollment);

        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($session->has('houseSelected')) {
                $houseObject = $houseManager->findHouseBy(array('id' => $session->get('houseSelected')));
                $enrollment->setHouse($houseObject);
            }

            $enrollmentManager->updateEnrollment($enrollment);
            $this->setFlash('success', 'Datele tale au fost trimise cu succes. Urmareste pasii detaliati in pe email pentru a te inscrie in campanie.');

            return $this->render('AppBundle:Default:enrollment_success.html.twig');
        }

        return $this->render('AppBundle:Default:enrollment.html.twig', array(
            'enrollmentForm' => $form->createView(),
            'selectedHouse' => $selectedHouse,
            'generatedPhoto' => $generatedPhoto
        ));
    }

    private function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set($action, $value);
    }

    /**
     * @Route("/checkemail", name="check_email")
     */
    public function checkEmailAction(Request $request)
    {
        $email = $request->get('email', null);
        if (null === $email) {
            return new JsonResponse(false);
        }
        $enrollmentManager = $this->get('app.manager.enrollment_manager');

        if ($enrollmentManager->findEnrollmentBy(array('email' => $email))) {
            return new JsonResponse('Acest email a mai fost folosit.');
        } else {
            return new JsonResponse(true);
        }
    }

    /**
     * @Route("/checkphone", name="check_phone")
     */
    public function checkPhoneAction(Request $request)
    {
        $phone = $request->get('phone', null);
        if (null === $phone) {
            return new JsonResponse(false);
        }
        $enrollmentManager = $this->get('app.manager.enrollment_manager');

        if ($enrollmentManager->findEnrollmentBy(array('phone' => $phone))) {
            return new JsonResponse('Acest numar de telefon a mai fost folosit.');
        } else {
            return new JsonResponse(true);
        }
    }
}
