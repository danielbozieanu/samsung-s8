<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DashboardController extends Controller {

    /**
     * @Route("/dashboard", name="dashboard_index")
     */
    public function indexAction(Request $request) {
        $enrollmentManager = $this->getEnrollmentManager();

        $enrollments = $enrollmentManager->findEnrollments();

        return $this->render('AppBundle:Dashboard:enrollments.html.twig', array(
            'enrollments' => $enrollments,
        ));
    }

    /**
     * @Route("/dashboard/inscrieri", name="dashboard_inscrieri")
     */
    public function enrollmentsAction(Request $request){
        $enrollmentManager = $this->getEnrollmentManager();

        $enrollments = $enrollmentManager->findEnrollments();

        return $this->render('AppBundle:Dashboard:enrollments.html.twig', array(
            'enrollments' => $enrollments,
        ));
    }

    /**
     * @Route("/dashboard/cache/clear", name="dashboard_clear_cache")
     */
    public function clearCacheAction(Request $request) {
        $kernel = $this->get('kernel');

        $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
        $application->setAutoExit(false);

        $options = array('command' => 'cache:clear', '--env' => 'prod', '--no-warmup' => true);
        $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));

        return new RedirectResponse($this->generateUrl('dashboard_index'));
    }

    private function getEnrollmentManager() {
        return $this->get('app.manager.enrollment_manager');
    }

}
