<?php

namespace AppBundle\Manager;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Enrollment;

class EnrollmentManager {

    private $objectManager;
    private $class;
    private $repository;

    /**
     * Constructor.
     *
     * @param ObjectManager           $om
     * @param string                  $class
     */
    public function __construct(ObjectManager $om, $class) {

        $this->objectManager = $om;
        $this->repository = $om->getRepository($class);

        $metadata = $om->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    public function createEnrollment() {
        $class = $this->getClass();
        $enrollment = new $class;

        return $enrollment;
    }

    public function updateEnrollment(Enrollment $enrollment) {
        $this->objectManager->persist($enrollment);
        $this->objectManager->flush();
    }

    public function deleteEnrollment(Enrollment $enrollment) {
        $this->objectManager->remove($enrollment);
        $this->objectManager->flush();
    }

    public function findEnrollmentBy(array $criteria) {
        return $this->repository->findOneBy($criteria);
    }

    public function findEnrollments() {
        return $this->repository->findAll();
    }

    public function getClass() {
        return $this->class;
    }

}
