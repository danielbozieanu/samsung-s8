<?php

namespace AppBundle\Manager;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\House;

class HouseManager {

    private $objectManager;
    private $class;
    private $repository;

    /**
     * Constructor.
     *
     * @param ObjectManager           $om
     * @param string                  $class
     */
    public function __construct(ObjectManager $om, $class) {

        $this->objectManager = $om;
        $this->repository = $om->getRepository($class);

        $metadata = $om->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    public function createHouse() {
        $class = $this->getClass();
        $house = new $class;

        return $house;
    }

    public function updateHouse(House $house) {
        $this->objectManager->persist($house);
        $this->objectManager->flush();
    }

    public function deleteHouse(House $house) {
        $this->objectManager->remove($house);
        $this->objectManager->flush();
    }

    public function findHouseBy(array $criteria) {
        return $this->repository->findOneBy($criteria);
    }

    public function findHouses() {
        return $this->repository->findAll();
    }

    public function getClass() {
        return $this->class;
    }

}
