<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\Table(name="enrollment_media")
 * @Vich\Uploadable
 */
class EnrollmentMedia {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\JoinColumn(name="enrollment_id", referencedColumnName="id", nullable=true)
     * @ORM\ManyToOne(targetEntity="Enrollment")
     */
    protected $enrollment;

    /**
     * @Vich\UploadableField(mapping="upload", fileNameProperty="pictureName")
     */
    protected $picture;

    /**
     * @ORM\Column(name="picture_name", type="string", nullable=true)
     */
    protected $pictureName;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    public function getId() {
        return $this->id;
    }

    public function getEnrollment() {
        return $this->enrollment;
    }

    public function setEnrollment($enrollment) {
        $this->enrollment = $enrollment;
    }

    public function getPicture() {
        return $this->picture;
    }

    public function setPicture($picture) {
        $this->picture = $picture;

        if ($picture instanceof UploadedFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getPictureName() {
        return $this->pictureName;
    }

    public function setPictureName($pictureName) {
        $this->pictureName = $pictureName;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function getUpdatedAt() {
        return $this->updatedAt;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return EnrollmentMedia
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return EnrollmentMedia
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
