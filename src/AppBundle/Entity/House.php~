<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * House
 *
 * @ORM\Table(name="house")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HouseRepository")
 * @Vich\Uploadable
 */
class House
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;


    /**
     * @Vich\UploadableField(mapping="upload", fileNameProperty="pictureName")
     */
    protected $picture;

    /**
     * @ORM\Column(name="picture_name", type="string", nullable=true)
     */
    protected $pictureName;


    /**
     * @Vich\UploadableField(mapping="upload", fileNameProperty="flagName")
     */
    protected $flag;

    /**
     * @ORM\Column(name="flag_name", type="string", nullable=true)
     */
    protected $flagName;


    /**
     * @Vich\UploadableField(mapping="upload", fileNameProperty="frameName")
     */
    protected $frame;

    /**
     * @ORM\Column(name="frame_name", type="string", nullable=true)
     */
    protected $frameName;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return House
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set pictureName
     *
     * @param string $pictureName
     * @return House
     */
    public function setPictureName($pictureName)
    {
        $this->pictureName = $pictureName;

        return $this;
    }

    /**
     * Get pictureName
     *
     * @return string
     */
    public function getPictureName()
    {
        return $this->pictureName;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return House
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return House
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setPicture($picture)
    {
        $this->picture = $picture;

        if ($picture instanceof UploadedFile) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getPicture()
    {
        return $this->picture;
    }


    /**
     * Set flagName
     *
     * @param string $flagName
     * @return House
     */
    public function setFlagName($flagName)
    {
        $this->flagName = $flagName;

        return $this;
    }

    /**
     * Get flagName
     *
     * @return string
     */
    public function getFlagName()
    {
        return $this->flagName;
    }

    /**
     * Set frameName
     *
     * @param string $frameName
     * @return House
     */
    public function setFrameName($frameName)
    {
        $this->frameName = $frameName;

        return $this;
    }

    /**
     * Get frameName
     *
     * @return string
     */
    public function getFrameName()
    {
        return $this->frameName;
    }

    public function setFlag($flag)
    {
        $this->flag = $flag;

        if ($flag instanceof UploadedFile) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getFlag()
    {
        return $this->flag;
    }
    public function setFrame($frame)
    {
        $this->frame = $frame;

        if ($frame instanceof UploadedFile) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getFrame()
    {
        return $this->frame;
    }
}
